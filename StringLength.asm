.data
	str:	.asciiz		"MIPS is awesome!"


.text

	la $t0, str			# $t0 holds the string address
	li $t1, 0			# $t1 holds the character count
	
   loopTop:				# Top of our loop
   	lb $t2, 0($t0)			# Load the character at address $t0
   
   	bne $t2, $zero, notEqual	# Jump to notEqual if things aren't equal
   	
   	# Found our end of string
   	
   	li $v0, 1			# Setting syscall 1
   	move $a0, $t1 			# Issuing the system call
   	syscall
   	
   	li $v0, 10			# Setting syscall 10
   	syscall				# Issuing the system call
   	
   notEqual:
   	addi $t1, $t1, 1		# Increment $t1
   	addi $t0, $t0, 1		# Move to the next char
   	j loopTop			# Jump to the top of the loop