.data
	str:		.asciiz 	"Hello CMPSC 210!\n"

.text
	la $t0, str		# loads the string into register $t0.
	li $t1, ' '		# directly loads ' ' into register $t1.
	li $t2, '+'		# directly loads '+' into register $t2.
	
   LOOP:
	lbu $t3, 0($t0)		# go to where $t0 points (the beginning of the string) and grab the 
	                        # bit, use load bit unsigned instruction, grab bit that $t0 points at
	                        # and put it in to $t3.
	bne $t1, $t3, NEXT
	sb $t2, 0($t0)
		
   NEXT:
   	addi $t0, $t0, 1
	bne $t3, $zero, LOOP
	
   EXIT:
   	li $v0, 4
   	la $a0, str
   	syscall
   
  	li $v0, 10
  	syscall
