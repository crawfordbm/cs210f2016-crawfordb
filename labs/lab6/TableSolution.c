#include <stdio.h>
#include <math.h>

int main() {

	int n;
	int x;
	float math, math2, math3, math4;	

	// Finds 1/i^3.
    	math = 1/(pow(x,3));

	// Finds sqrt(i).
	math2 = sqrt(x);

	// Finds log_3(x).
	math3 = log(x)/log(3);

	// Finds 1.2^i.
	math4 = pow(1.2,x);
	
	printf("Which column should appear first in the table?");
	printf("\n1. 1/x^3");
	printf("\n2. sqrt(x)");
	printf("\n3. log_3 x");
	printf("\n4. 1.2^x\n");

	scanf("%d", &n);

	/*if (n = 1){
		printf(math);
    	} else if (n = 2){
		printf(math2);
	} else if (n = 3){
		printf(math3);
	} else if (n = 4){
		printf(math4);
	} //if*/

	int max, i;
	printf("\nEnter an integer between 1 and 50:");
	scanf("%d",&max);

	printf("\t x\t  1/x^3  \t sqrt(x)  \tlog_3(x)\t     1.2^x\n");
	printf("\t -\t  -----  \t -------  \t--------\t     -----\n");
	for (i = 1; i <= max; i++) {
        	printf("\t%2d \t %1.5f \t %1.5f \t %1.5f \t %10.5f\n", i, pow(i,-3), sqrt(i), log(i)/log(3), pow(1.2,i));
	} //for

	return 0;
} //main
