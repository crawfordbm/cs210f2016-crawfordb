#include <stdio.h>
#include <math.h>

int main()
{
	int n;
	int i = 1;
    float math, math2, math3, math4;

	printf("Please enter a number between 1 and 50: ");

	// Scans in the number the user enters.
	scanf("%d", &n);

	// Prints the column headers.
	printf("\nx\t 1/x^3\t\t sqrt(x)\t log_3(x)\t 1.2^x\n");

	printf("-\t -----\t\t -------\t --------\t -----\n");

    while (i < n+1){
    	
    	// Finds 1/i^3.
    	math = 1/(pow(i,3));

		// Finds sqrt(i).
		math2 = sqrt(i);

		// Finds log_3(x).
		math3 = log(i)/log(3);

		// Finds 1.2^i.
		math4 = pow(1.2,i);
    
    	// Prints out the results of each function.
    	printf("%d\t %.5f\t %.5f\t %.5f\t %.5f\n", i, math, math2, math3, math4);
    	
    	i++;
	} // while

    return 0;

} // main

	


